w3c-trace-propagation-browsers

An implementation of [W3C Trace Context](https://www.w3.org/TR/trace-context) for client-side javascript web requests.