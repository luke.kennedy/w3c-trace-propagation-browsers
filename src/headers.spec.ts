import { assert } from 'chai';
import * as headers from "./headers";

describe("Headers", () => {
	describe("Adding headers to request", () => {
		it("Adds the headers correctly with a valid request", async () => {
			// Example values (input and output) from the spec
			let formattedHeader = headers.FormatTraceParent(new Uint8Array([0x4b, 0xf9, 0x2f, 0x35, 0x77, 0xb3, 0x4d, 0xa6, 0xa3, 0xce, 0x92, 0x9d, 0x0e, 0x0e, 0x47, 0x36]), new Uint8Array([0x00, 0xf0, 0x67, 0xaa, 0x0b, 0xa9, 0x02, 0xb7]), 0x01);
			assert.strictEqual(formattedHeader, "00-4bf92f3577b34da6a3ce929d0e0e4736-00f067aa0ba902b7-01")
		})
	})
})