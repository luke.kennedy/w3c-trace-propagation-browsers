// These are all-lower-case at the direction of the standard
export const TraceParentHeaderName = "traceparent"
export const TraceStateHeaderName = "tracestate"
export const VendorName = "llkennedyw3trace"

/**
 * FormatTraceParent takes the individual components of the traceparent header and formats them as a completed string
 * @param {byte} version Only 0x00 is currently supported
 * @param {Uint8Array} traceID 
 * @param {Uint8Array} parentID 
 * @param {byte} traceFlags Only the LSB is currently supported, set to 1 to indicate that this trace has been chosen for recording here or upstream
 * @returns {string} Formatted string
 */
export function FormatTraceParent(traceID: Uint8Array, parentID: Uint8Array, traceFlags: number = 0x00, version: number = 0x00): string {
	// Version 0 is the only version in the current standard
	if (version === 0) {
		return [paddedHexString(version), version00Format(traceID, parentID, traceFlags)].join("-").toLowerCase();
	}
	// 0xFF is explicitly invalid, and anything greater is outside the range of a valid byte
	if (!(version >= 0 && version <= 0xFF)) {
		throw new Error("Versions 0xFF and greater are invalid")
	}
	// This is a valid version number, but it's for a version not currently released at time of writing
	throw new Error("Versions other than 0x00 not currently supported")
}

/** This is a generic tracing system, we don't pass any custom information in tracestate
 * 
 * For that reason, we just copy the parentID string from traceparent, as indicated
 * in the spec. We should probably copy the finished string rather than re-doing the
 * validation and formatting work, but that's a TODO
 */
export function FormatTraceState(parentID: Uint8Array): string {
	if (parentID?.length !== 8) {
		throw new Error("parent-id must be exactly 8 bytes of data")
	}
	let foundNonZero = false;
	for (let i = 0; i < parentID.length; i++) {
		if (parentID[i] !== 0) {
			foundNonZero = true;
			break;
		}
	}
	if (!foundNonZero) {
		throw new Error("parent-id cannot be all-zeroes")
	}
	let outputElements: string[] = [VendorName];
	// Construct hex of parent ID
	let parentIDHexElements: string[] = [];
	for (let i = 0; i < 8; i++) {
		parentIDHexElements.push(paddedHexString(parentID[i]));
	}
	outputElements.push(parentIDHexElements.join(""));
	return outputElements.join("=");
}

function version00Format(traceID: Uint8Array, parentID: Uint8Array, traceFlags: number): string {
	// Input validation
	if (traceFlags !== 0x00 && traceFlags !== 0x01) {
		throw new Error("trace-flags must be either 0x00 or 0x01 by currently supported standards")
	}
	if (traceID?.length !== 16) {
		throw new Error("trace-id must be exactly 16 bytes of data")
	}
	if (parentID?.length !== 8) {
		throw new Error("parent-id must be exactly 8 bytes of data")
	}
	// Validate that traceID and parentID are not all-zeroes (invalid)
	let foundNonZero = false;
	for (let i = 0; i < traceID.length; i++) {
		if (traceID[i] !== 0) {
			foundNonZero = true;
			break;
		}
	}
	if (!foundNonZero) {
		throw new Error("trace-id cannot be all-zeroes")
	}
	foundNonZero = false;
	for (let i = 0; i < parentID.length; i++) {
		if (parentID[i] !== 0) {
			foundNonZero = true;
			break;
		}
	}
	if (!foundNonZero) {
		throw new Error("parent-id cannot be all-zeroes")
	}
	let outputElements: string[] = [];
	// Construct hex of trace ID
	let traceIDHexElements: string[] = [];
	for (let i = 0; i < 16; i++) {
		traceIDHexElements.push(paddedHexString(traceID[i]));
	}
	outputElements.push(traceIDHexElements.join(""));
	// Construct hex of parent ID
	let parentIDHexElements: string[] = [];
	for (let i = 0; i < 8; i++) {
		parentIDHexElements.push(paddedHexString(parentID[i]));
	}
	outputElements.push(parentIDHexElements.join(""));
	// Construct hex of trace flags
	outputElements.push(paddedHexString(traceFlags))
	// Return the results joined with dashes
	return outputElements.join("-");
}

function paddedHexString(num: number): string {
	let raw = num?.toString(16) ?? "";
	if (raw.length % 2 !== 0) {
		return ["0", raw].join("")
	}
	return raw;
}