/** Any header object should conform to at least one of these shapes */
type HeaderObjectShapes = {
	[key: string]: string
} |
{
	[key: string]: string[]
} |
{
	[key: string]: string | string[]
};

export function getOriginalRNGSource(): (length: number) => Uint8Array {
	if (window?.crypto?.getRandomValues === undefined) {
		// We must be in Node
		const crypto = require('crypto');
		return (length: number) => crypto.randomBytes(length) as Uint8Array;
	} else {
		// Use browser crypto
		return (length: number) => window.crypto.getRandomValues(new Uint8Array(length));
	}
}

var rngSource: (length: number) => Uint8Array = getOriginalRNGSource();

export function SetRNGSource(newSource: (length: number) => Uint8Array) {
	rngSource = newSource;
}

/**
 * GenerateW3CTraceHeader generates a new trace/parent ID and mutates the headerObject
 * to contain those values as the correct W3C trace propagation headers.
 * @param headerObject The object to mutate
 */
export function GenerateW3CTraceHeader(headerObject: HeaderObjectShapes) {
	let traceID = rngSource(16);
	let parentID = rngSource(8);
	AddW3CTraceHeader(headerObject, traceID, parentID);
}


export function AddW3CTraceHeader(headerObject: HeaderObjectShapes, traceID: Uint8Array, parentID: Uint8Array) {

}